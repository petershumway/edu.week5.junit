package edu.week5.junit;

// The purpose of this program is to demonstrate the basic uses of assertions through JUnit in Java. The Methods are
// designed to return a certain object, number, or string, etc. The assertions will throw an error and cause the
// program to crash if the returned value is not what was expected. This is very helpful as you write a method with a
// return value. You can simply create an assertion confirming the value you need returned and then write your method
// with the ability to test right away and the whole way through beginning to end.

public class JUnitExamples {

    // assertArrayEquals
    static public int[] exampleArrayEquals() {

        // We want to create an array with 5 numbers in it.
        // Our JUnit test will make sure we are returning a length of 5
        System.out.println("Lets add 5 numbers to an array so its filled like this {1,2,3,4,5}:");
        int[] arrayA = new int[5];

        boolean loop = true;
        do {
            // loop through and add 5 numbers
            for (int i = 0; i < 5; i++) {
                System.out.println("Adding " + (i + 1));
                arrayA[i] = (i + 1);

            }
            if (arrayA.length == 5) {
                loop = false;
            }
            else {
                arrayA = new int[5];
                loop = true;
            }
        }while (arrayA.length != 5);
        // we return the array with a length of 5 and with {1,2,3,4,5}.
        // our JUnit test will crash if we return anything else.

        System.out.println("Checking the array length...");
        System.out.println("Length = " + arrayA.length);
        return arrayA;
    }

    // assertEquals
    static public int exampleEquals(int value) {
        System.out.println("This method rounds down to 0 or up to 100.");
        System.out.println("The entered number is: " + value);
        int zero = 0;
        int oneHundred = 100;

        if (value < 50) {
            System.out.println("It is rounded down to 0.");
            return zero;
        }
        else {
            System.out.println("It is rounded up to 100.");
            return oneHundred;
        }
    }

    // assertFalse
    static public boolean exampleFalse(String something) {

        String happiness = "Happiness";

        System.out.println("This method compares a given String to the word 'Happiness'");
        System.out.println("We will test the word '" + something + "'.");

        // this will only work if the string entered is 'Happiness' and it will crash the program.
        if (happiness == something) {
            System.out.println("Wow! Only " + something + " == Happiness");
            return true;
        }
        else {
            System.out.println("False: " + something + " != Happiness");
            return false;
        }
    }

    // assertTrue
    static public boolean exampleTrue(String name) {

        String god = "love";
        String spiritChild = name;

        System.out.println("This method checks to see if God loves a given individual");
        System.out.println("We will see if God loves " + name + ".");

        // This is intended to always return true.
        if (god == "love") {
            name = "loved";
            if (name == "loved") {
                System.out.println("True: Of course God loves " + spiritChild + "!");
                return true;
            }
            else {
                name = "loved";
                System.out.println("True: Of course God loves " + spiritChild + "!");
                return true;
            }
        }
        else {
            System.out.println("Logic Error: God loves everyone so there must be a human mistake here so lets fix it.");
            name = "loved";
            if (name == "loved") {
                System.out.println("True: Of course God loves " + spiritChild + "!");
                return true;
            }
            else {
                name = "loved";
                System.out.println("True: Of course God loves " + spiritChild + "!");
                return true;
            }
        }
    }

    // assertNotNull
    static public String exampleNotNull(String nullValue) {

        String fullValue = "Full Value";

        if (nullValue == null) {
            // we want to be sure that our nullValue becomes not null
            nullValue = fullValue;
        }

        System.out.println("This method turns Null Values into " + fullValue + "'s!");
        return nullValue;
    }

    // assertNull
    static public String exampleNull(String service) {
        System.out.println("This method allows you to give service and expect nothing in return.");
        String reward = "money";
        if (service != null) {
            reward = null;
        }
        return reward;
    }

    // assertNotSame
    static public String exampleNotSame(String same) {

        System.out.println("This method tests if '" + same + "' and 'same' are the same.");

        if ("same" == same) {
            System.out.println("Opps there was a type it should have been 'Same'");
            same = "Same";
        }

        System.out.println("Although 'Same' & 'same' seem to be the same they are actually not the same.");

        return same;
    }

    // assertSame
    static public int exampleSame(int num1, int num2) {
        System.out.println("This method takes two numbers and adds them together and makes sure they equal 10");
        int total = num1 + num2;

        if (total == 10 ) {
            System.out.println(num1 + " + " + num2 + " = 10");
            return total;
        }
        else if (total > 10) {
            System.out.println(num1 + " + " + num2 + " = Too much!");
            int difference = total - 10;
            System.out.println("Opps the numbers dont add to 10 so we deducted " + difference + " to fix it.");
            return total - difference;
        }
        else {
            System.out.println(num1 + " + " + num2 + " = Not enough!");
            int difference = 10 - total;
            System.out.println("Opps the numbers dont add to 10 so we added " + difference + " to fix it.");
            return total + difference;
        }
    }

    // assertThat
    static public String exampleThat(int num1, int num2) {

        System.out.println("for this methd we will take " + num1 + " and " + num2 + " and make them into a string.");
        String toString = num1 + ", " + num2;
        System.out.println("Our String looks like this '" + toString + "'.");

        return toString;

    }

    public static void main(String[] args) {

        // if any of the following tests fail then the program will crash because we are testing the assertions.
        System.out.println("This program demonstrates the use of jUnit assertions. If any of the following tests fail " +
                "then the program will crash due to the assertion error.\n");


        System.out.println("#####Array Equals Test#####\n");
        exampleArrayEquals();
        System.out.println("If we made it this far then success!");

        System.out.println("\n#####Equals Test#####\n");
        exampleEquals(93);
        System.out.println("If we made it this far then success!");

        System.out.println("\n#####False Test#####\n");
        exampleFalse("Power");
        System.out.println("Success! We would have crashed if that were true.");

        System.out.println("\n#####True Test#####\n");
        exampleTrue("You");
        System.out.println("Success! We would not exist if that were false.");

        System.out.println("\n#####Not Null Test#####\n");
        exampleNotNull(null);
        System.out.println("Success! This is one of those few times where you get something for nothing.");

        System.out.println("\n#####Null Test#####\n");
        exampleNull("service");
        System.out.println("Success! You gave service and got nothing in return, as it should be.");

        System.out.println("\n#####Not Same Test#####\n");
        exampleNotSame("Same");
        System.out.println("Success! They were similar but not the same.");


        System.out.println("\n#####Same Test#####\n");
        exampleSame(3, 25);
        System.out.println("Success! This way any two numbers can equal 10.");

        System.out.println("We made it to the end and havent crashed which means our return values are all correct" +
                "and our assertions have all passed.");


    }
}
