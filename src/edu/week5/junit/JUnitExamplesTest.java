package edu.week5.junit;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class JUnitExamplesTest {

    @Test
    void testArrayEquals() {
        // The method needs to return an array with the size of 5
        int[] array = {1,2,3,4,5};

        assertArrayEquals(array, JUnitExamples.exampleArrayEquals());
        System.out.println("Success!");
    }

    @Test
    void testEquals() {
        // number below 50
        int num1 = 37;

        // number above 50
        int num2 = 85;
        System.out.println("Test 1:");
        assertEquals(0, JUnitExamples.exampleEquals(num1));
        System.out.println();
        System.out.println("Test 2:");
        assertEquals(100, JUnitExamples.exampleEquals(num2));

        System.out.println("Success!");
    }

    @Test
        void testFalse() {
        // test if a word equals happiness
        String money = "Money";
        assertFalse(JUnitExamples.exampleFalse(money));
        System.out.println("We would have crashed if that were true.");
    }

    @Test
    void testTrue() {
        // test if the given person is loved by God
        String name = "Peter";
        assertTrue(JUnitExamples.exampleTrue(name));
        System.out.println("We would not exist if that were false.");
    }

    @Test
    void testNotNull() {
        // A test to be sure a value is not Null
        String value = null;

        // we pass a null value in and a not null value should come back.
        assertNotNull(JUnitExamples.exampleNotNull(value));
        System.out.println("Success! If the value was Null then this would have crashed.");
    }

    @Test
    void testNull() {
        // A test to be sure a value is not Null
        String give = "service";

        // we pass a null value in and a not null value should come back.
        assertNull(JUnitExamples.exampleNull(give));
        System.out.println("Success! You gave service and got nothing in return as should be expected.");
    }

    @Test
    void testNotSame() {
        String sameUpper = "Same";
        String sameLower = "same";

        // we pass a String object into the assertion and match it to the result.
        assertNotSame(sameLower, JUnitExamples.exampleNotSame(sameUpper));
        System.out.println("Success! We make sure it always is the same.");
    }

    @Test
    void testSame() {

        // we pass a String object into the assertion and match it to the result.
        assertSame(10, JUnitExamples.exampleSame(1,9));
        System.out.println("Success! We made sure our answers were always 10.");
    }

}